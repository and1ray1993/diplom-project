export const mobileMenu = () => {
  const burg = $('.js-burger');
  burg.click(() => {
    if ($('.js-lvl1-nav').css('display') === 'none') {
      $('.js-lvl1-nav').css('display', 'block');
    } else {
      $('.js-lvl1-nav').css('display', 'none');
    }
  });

  accordeon('.js-lvl1', '.js-lvl1-nav');
  accordeon('.js-lvl2', '.js-lvl2-nav');
  accordeon('.js-lvl3', '.js-lvl3-nav');

  function accordeon(link, nav) {
    $(link).click(function (e) {
      e.preventDefault();
      var menu = $(this).closest(nav);
      if (false == $(this).next().is(':visible')) {
        menu.find('li').removeClass('slide active');
        menu.find('ul').slideUp();
      }
      $(this).next().slideToggle();
      $(this).parent().addClass('slide');
    });
  }
}