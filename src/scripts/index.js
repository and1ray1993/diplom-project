import {mobileMenu} from "./mobileMenu";
import {desctopMenu} from "./desctopMenu";

$(document).ready(() => {

  Vue.use(window.vuelidate.default);
  Vue.use(VueMask.VueMaskPlugin);

  const required = validators.required;
  const email = validators.email;
  const minLength = validators.minLength;
  const sameAs = validators.sameAs;

  if ($('#subscribe').length > 0) {
    new Vue({
      el: '#subscribe',
      delimiters: ['${', '}'],
      data: {
        email: '',
      },
      methods: {},
      validations: {
        email: {
          email,
          required
        }
      }
    });
  }

  new Vue({
    el: '#question',
    delimiters: ['${', '}'],
    data: {
      email: '',
      question: ''
    },
    methods: {},
    validations: {
      email: {
        email,
        required
      },
      question: {
        required
      },
    }
  });

  new Vue({
    el: '#oneClick',
    delimiters: ['${', '}'],
    data: {
      email: '',
      phone: '',
      mask: '+7(###)###-##-##'
    },
    methods: {},
    validations: {
      email: {
        email,
        required
      },
      phone: {
        required
      },
    }
  });

  new Vue({
    el: '#reg',
    delimiters: ['${', '}'],
    data: {
      name: '',
      email: '',
      phone: '',
      pass: '',
      loopPass: '',
      mask: '+7(###)###-##-##'
    },
    methods: {},
    validations: {
      email: {
        email,
        required
      },
      phone: {
        required
      },
      name: {
        required
      },
      pass: {
        required,
        minLength: minLength(6)
      },
      loopPass: {
        required,
        sameAs: sameAs('pass')
      }
    }
  });

  new Vue({
    el: '#auth',
    delimiters: ['${', '}'],
    data: {
      email: '',
      pass: '',
    },
    methods: {},
    validations: {
      email: {
        email,
        required
      },
      pass: {
        required,
        minLength: minLength(6)
      }
    }
  });


  const sliderItem = $('.js-slider');
  const sliderNavItem = $('.js-nav-slider');
  const saleSlider = $('.js-sale-slider');

  sliderItem.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.js-nav-slider',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true,
          centerMode: true
        }
      }
    ]
  });


  sliderNavItem.slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.js-slider',
    focusOnSelect: true,
    arrows: true,
    prevArrow: '.js-arrow-prev',
    nextArrow: '.js-arrow-next',
  });

  sliderItem.on('afterChange', function checkCurrent() {
    const $slides = $('.js-item');
    const $navSlides = $('.js-nav-item');

    $slides.each((i, item) => {
      if ($(item).hasClass('slick-current')) {

        const index = $(item).children().attr('data-index');

        $navSlides.each((i, item) => {
          $(item).removeClass('slick-current');
          if (index === $(item).children().attr('data-index')) {
            $(item).addClass('slick-current');
          }
        });

      }
    });
  });


  if (window.outerWidth < 1200) {
    sliderItem.slick('unslick');
    sliderNavItem.slick('unslick');

    saleSlider.slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            infinite: true,
            centerMode: true
          }
        }
      ]
    });

    sliderItem.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true
    });
  }

  if (window.outerWidth < 750) {
    saleSlider.slick('unslick');

    saleSlider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      infinite: true,
      centerMode: true
    });
  }


  //popups

  $('.js-entrance').click(() => window.popupAuth());
  $('.js-bag').click(() => window.popupItems());
  $('.js-call-me').click(() => window.popupQuestion());
  $('.js-one').click(() => window.popupOne());
  $('.js-reg').click(() => window.popupReg());

  //для вызова модалок глобально из консоли
  window.popupReg = () => popup('.js-popup-reg');
  window.popupAuth = () => popup('.js-popup-auth');
  window.popupOne = () => popup('.js-popup-one');
  window.popupQuestion = () => popup('.js-popup-question');
  window.popupItems = () => popup('.js-popup-items');

  // function popups
  function popup(classPopup) {
    const $popup = $(classPopup);
    const $closeBtnPopup = $('.js-close-popup');
    $popup.addClass('popup_active');
    $closeBtnPopup.click(() => $popup.removeClass('popup_active'));
  }

  //check
  let check = $('.js-check');
  check.click(() => {
    let bool = check.attr('data-active');
    bool ? check.attr('data-active', '') : check.attr('data-active', 1);
  });

  //nav desctop
  if (window.outerWidth > 1200) {
    desctopMenu();
  }

  //nav mobile
  if (window.outerWidth < 1200) {
    mobileMenu();
  }

  //btn Up
  $('.js-btn-up').click(function () {
    $('html, body').animate({scrollTop: 0}, 500);
  });

  //redirect
  $('.js-redirect').click(() => {
    window.location.pathname = '/hope/item.html'
  });
  $('.js-home').click(() => {
    window.location.pathname = '/hope'
  });


  $('.js-filter').each((i, item) => {
    let text = $(item).find('p').html();

    if (window.outerWidth > 1200) {
      $(item).hover(() => {
        $('.js-filter-item').each((j, el) => {
          if (i === j) {
            $(el).css('display', 'block');
          }
        });
      }, () => {
        $('.js-filter-item').each((j, el) => {
          if (i === j) {
            $(el).css('display', 'none');
          }
        });
      });
    }

    $(item).click((e) => {
      if (e.target.className === 'filter__cat js-filter' && window.outerWidth < 1200) {

        if ($(item).find('.js-filter-item').css('display') == 'block') {
          $(item).find('.js-filter-item').css('display', 'none');
        } else if ($(item).find('.js-filter-item').css('display') == 'none') {
          $(item).find('.js-filter-item').css('display', 'block');
        }
      }

      if (e.target.className !== 'filter__cat js-filter') {
        $(item).find('p').html(e.target.textContent);
      }

      if (e.target.textContent === 'Сброс') {
        $(item).find('p').html(text);
      }
    });
  });

  //lazyLoad
  function lazy(el) {
    new LazyLoad({
      elements_selector: el,
      threshold: $(window).height(),
      callback_load() {
        if ($(window).data('plugin_stellar')) {
          $(window)
            .data('plugin_stellar')
            .refresh();
        }
      }
    });
  }

  lazy('.js-lazy');
});
