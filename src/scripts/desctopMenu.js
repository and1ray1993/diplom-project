export const desctopMenu = (arg, arg2) => {
  const navElements = $('.js-nav');
  const navSubmenu = $('.js-submenu');
  const nav = $('.nav');

  window.call = function () {
    nav.attr('data-active', '1');
  };
  window.callSec = function () {
    nav.attr('data-active', '');
  };

  const navElementsLvl2 = $('.js-nav-lvl2');
  const navSubmenuLvl2 = $('.js-submenu-lvl2');

  const navElementsLvl3 = $('.js-nav-lvl3');
  const navSubmenuLvl3 = $('.js-submenu-lvl3');


  /**
   * Выдает ошибку но отрабатывает???   ↓↓↓           ↓↓↓ */
  descNav(navElements, navSubmenu, window.call, window.callSec);
  descNav(navElementsLvl2, navSubmenuLvl2);
  descNav(navElementsLvl3, navSubmenuLvl3);

  function descNav(links, submenu, call, call2) {
    links.each((i, item) => {
      $(item).hover(() => {
          submenu.each((j, el) => {
            if (i === j) {
              $(el).css('display', 'block');
            }
          });
          call();
        },
        () => {
          submenu.each((j, el) => {
            if (i === j) {
              $(el).css('display', 'none');
            }
          });
          call2();
        });
    });
  }
};